<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 22:40
 */

require_once "vendor/autoload.php";

use App\Kernel\Kernel;

$kernel = new Kernel();
$kernel->createDB();
