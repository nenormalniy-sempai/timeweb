<?php
require_once "vendor/autoload.php";

use App\Kernel\Kernel;

$kernel = new Kernel();
$kernel->init();
$kernel->routing();