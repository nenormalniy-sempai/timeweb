<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 14:43
 */

namespace App\Model;

use App\Client\ClientInterface;
use App\Kernel\Interfaces\RequestInterface;
use App\Model\Db\Result;
use App\Parser\ParserFactory;
use App\Parser\ResultDTO;

class Search
{
    const TYPE_LINKS = 1;
    const TYPE_IMAGES = 2;
    const TYPE_TEXTS = 3;

    private const TYPE_NAMES = [
        self::TYPE_LINKS => 'ссылок.',
        self::TYPE_IMAGES => 'картинок.',
        self::TYPE_TEXTS => 'текста.',
    ];

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $type;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * Search constructor.
     *
     * @param RequestInterface $request
     * @param ClientInterface $client
     */
    public function __construct(RequestInterface $request, ClientInterface $client)
    {
        $this->prepareUrl($request);
        $this->text = $request->getPostParam('text-search');
        $this->type = $request->getPostParam('criteria-type');
        $this->client = $client;
    }

    /**
     * Поиск
     *
     * @return ResultDTO
     */
    public function search(): ResultDTO
    {
        if (!$this->isValidSearch()) {
            return new ResultDTO();
        }

        $response = $this->client->sendRequest($this->url);

        if (empty($response)) {
            return new ResultDTO();
        }

        $parser = ParserFactory::getParser($this->type, $this->url, $this->text);
        $result = $parser->parse($response);

        if (empty($result)) {
            return $result;
        }

        $this->saveData($result);

        return $result;
    }

    /**
     * Валидация поиска
     *
     * @return bool
     */
    private function isValidSearch(): bool
    {
        if ($this->isInvalidType()
            || $this->isInvalidUrl()
            || $this->isInvalidTextSearch()
        ) {
            return false;
        }

        return true;
    }

    /**
     * Проверяет, что при поиске по тексту заполнено поле text
     *
     * @return bool
     */
    public function isInvalidTextSearch(): bool
    {
        return $this->type == self::TYPE_TEXTS && empty($this->text);
    }

    /**
     * Проверяет url
     *
     * @return bool
     */
    private function isInvalidUrl(): bool
    {
        return empty($this->url) || !filter_var($this->url, FILTER_VALIDATE_URL);
    }

    /**
     * Проверяет тип
     *
     * @return bool
     */
    private function isInvalidType(): bool
    {
        return empty($this->type) || !in_array($this->type, [self::TYPE_LINKS, self::TYPE_IMAGES, self::TYPE_TEXTS]);
    }

    /**
     * Подготовка урла
     *
     * @param RequestInterface $request
     */
    private function prepareUrl(RequestInterface $request)
    {
        $url = $request->getPostParam('url');
        if (preg_match('#^http(s*)://#', $url)) {
            $this->url = $url;
        } else {
            $this->url = "http://" . $url;
        }
    }


    /**
     * Сохраняет результаты поиска
     *
     * @param ResultDTO $result
     */
    public function saveData(ResultDTO $result)
    {
        $model = new Result();
        $model->url = $result->getUrl();
        $model->count = $result->getCount();
        $model->type = $this->type;
        $model->data = json_encode($result->getElements());
        $model->insert();
    }

    /**
     * Возвращает название типа поиска
     *
     * @param int $type
     * @return string
     */
    public static function getTypeName(int $type): string
    {
        if (empty(self::TYPE_NAMES[$type])) {
            return 'неопознан';
        }

        return self::TYPE_NAMES[$type];
    }
}