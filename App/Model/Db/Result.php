<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 23:09
 */

namespace App\Model\Db;

use App\Config\DbConfig;
use App\Parser\ResultDTO;

class Result
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $data;

    /**
     * @var string
     */
    public $url;

    /**
     * @var int
     */
    public $count;

    /**
     * @var int
     */
    public $type;

    /**
     * Вставляет запись в таблицу
     */
    public function insert()
    {
        $dbh = DbConfig::getPDO(true);
        $data = [
            ':urlResult' => $this->url,
            ':countResult' => $this->count,
            ':dataResult' => $this->data,
            ':typeResult' => $this->type,
        ];

        $stmt = $dbh->prepare('INSERT INTO results (url, `count`, `data`, `type`) VALUES (:urlResult, :countResult, :dataResult, :typeResult)');
        $stmt->execute($data);
    }

    /**
     * Возвращает все результаты поисков
     *
     * @return array
     */
    public static function getAll(): array
    {
        $result = [];
        $dbh = DbConfig::getPDO(true);
        $stmt = $dbh->query('SELECT id, url, `count`, `type` FROM results ORDER BY id DESC');

        while ($row = $stmt->fetch()) {
            $result[$row['id']] = [
                'url' => $row['url'],
                'count' => $row['count'],
                'type' => $row['type'],
            ];
        }

        return $result;
    }

    /**
     * Получаем запись по id
     *
     * @param int $id
     * @return ResultDTO
     */
    public static function getFromId(int $id): ResultDTO
    {
        $result = new ResultDTO();
        $dbh = DbConfig::getPDO(true);
        $stmt = $dbh->prepare('SELECT `data`, url, `count` FROM results WHERE id = :id ORDER BY id DESC LIMIT 1');
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        if (empty($row)) {
            return $result;
        }

        $result->setUrl($row['url']);
        $result->setCount($row['count']);
        $result->setElements(json_decode($row['data'], true));

        return $result;
    }
}