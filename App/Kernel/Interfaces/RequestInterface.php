<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 11:21
 */

namespace App\Kernel\Interfaces;

interface RequestInterface
{
    public function init();
    public function getRoute(): string;
    public function getPostParam(string $key);
}