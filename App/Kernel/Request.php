<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 10:24
 */

namespace App\Kernel;

use App\Kernel\Interfaces\RequestInterface;

class Request implements RequestInterface
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $route;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $params = [];

    /**
     * Возвращает по ключу данные из пост запроса
     *
     * @param string $key
     * @return string|null
     */
    public function getPostParam(string $key): ?string
    {
        if (empty($this->data[$key])) {
            return null;
        }

        return $this->data[$key];
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute(string $route): void
    {
        $this->route = $route;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setParams(string $key, string $value): void
    {
        $this->params[$key] = $value;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return array
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function init()
    {
        $this->prepare();
        $this->setData($_POST);
    }

    private function prepare()
    {
        $this->setUri($_SERVER['REQUEST_URI']);
        $this->checkQuery();
    }

    /**
     * Собирает строку запроса
     */
    private function checkQuery()
    {
        if (strripos($_SERVER['REQUEST_URI'], '?') !== false) {
            $query = explode('?', $_SERVER['REQUEST_URI']);
            $this->setRoute($query[0]);
            if (strripos($query[1], '&') !== false) {
                $params = explode('&', $query[1]);
                foreach ($params as $param) {
                    $this->setParam($param);
                }
            } else {
                $this->setParam($query[1]);
            }
        } else {
            $this->setRoute($_SERVER['REQUEST_URI']);
        }
    }

    /**
     * Складывает параметры из гет запроса
     *
     * @param string $param
     */
    private function setParam(string $param)
    {
        if (strripos($param, '=') !== false) {
            $param = explode('=', $param);
            $this->setParams($param[0], $param[1]);
        }
    }

    /**
     * Возвращает параметр по ключу
     *
     * @param string $key
     * @return mixed|null
     */
    public function getParam(string $key): ?string
    {
        if (empty($this->params[$key])) {
            return null;
        }

        return $this->params[$key];
    }
}