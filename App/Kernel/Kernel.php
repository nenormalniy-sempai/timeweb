<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 15.08.19
 * Time: 18:06
 */

namespace App\Kernel;

use App\Config\ConfigLoader;
use App\Config\DbCreator;
use Dotenv\Dotenv;

class Kernel
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Router
     */
    private $router;

    /**
     * Инициализация ядра
     */
    public function init()
    {
        $this->initEnvironment();
        $this->initRouter();
        $this->initRequest();
    }

    private function initEnvironment()
    {
        $dotenv = Dotenv::create(__DIR__ . '/../../');
        $dotenv->load();
        ConfigLoader::init();
    }

    /**
     * Инициализация роутинга
     */
    private function initRouter()
    {
        $this->router = new Router();
    }

    /**
     * Получение реквеста из запроса
     */
    private function initRequest()
    {
        $this->request = new Request();
        $this->request->init();
    }

    /**
     * Роутинг
     */
    public function routing()
    {
        $this->router->route($this->request);
    }

    /**
     * Создание бд
     */
    public function createDB()
    {
        try {
            $this->initEnvironment();
            $dbCreator = new DbCreator(ConfigLoader::getDbConfig());
            $dbCreator->createDb();
            echo 'База и таблица созданы.' . "\n";
        } catch (\Throwable $exception) {
            echo $exception->getMessage() . "\n";
        }
    }
}