<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 10:50
 */

namespace App\Kernel;

use App\Controller\{
    FrontController,
    ErrorController
};
use App\Kernel\Interfaces\RequestInterface;

class Router
{
    private const NOT_FOUND_CONTROLLER = 'Не найден контроллер: ';
    private const NOT_FOUND_ACTION = 'Не найден экшен: ';

    /**
     * @param RequestInterface $request
     * @return string
     */
    public function route(RequestInterface $request): string
    {
        $routes = explode('/', $request->getRoute());

        if ($this->isMain($routes)) {
            $controller = new FrontController();
            return $controller->indexAction();

        }

        return $this->getRoute($routes, $request);
    }

    /**
     * Проверяет, к основному ли контроллеру запрос
     *
     * @param array $routes
     * @return bool
     */
    private function isMain(array $routes): bool
    {
        return (count($routes) >= 2 && $routes[1] == '');
    }

    /**
     * Возвращает роут
     *
     * @param array $routes
     * @param RequestInterface $request
     * @return string
     */
    private function getRoute(array $routes, RequestInterface $request): string
    {
        $controllerName = 'App\\Controller\\' . ucfirst($routes[1]) . 'Controller';
        $action = 'indexAction';

        if (!empty($routes[2])) {
            $action = $routes[2] . 'Action';
        }

        if (!class_exists($controllerName)) {
            $this->getErrorRoute(self::NOT_FOUND_CONTROLLER . $routes[1]);
        }

        $controller = new $controllerName();

        if (!method_exists($controller, $action)) {
            return $this->getErrorRoute(self::NOT_FOUND_ACTION . $routes[2]);
        }

        return $controller->$action($request);
    }

    private function getErrorRoute(string $message): string
    {
        $controller = new ErrorController();
        return $controller->error404Action($message);
    }
}