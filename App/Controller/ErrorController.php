<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 12:35
 */

namespace App\Controller;

class ErrorController extends AbstractController
{
    public function error404Action(string $message): string
    {
        return $this->viewBuilder->generateView('404_view.php', 'template_view.php', $message);
    }
}