<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 12:40
 */

namespace App\Controller;

use App\Kernel\Request;
use App\Model\Db\Result;
use App\Parser\ResultDTO;

class ResultController extends AbstractController
{
    const RESULT_URL = 'result';
    const RESULT_ROW_URL = 'result/getRow';

    public function indexAction(): string
    {
        return $this->viewBuilder->generateView('results_view.php', 'template_view.php', Result::getAll());
    }

    public function getRowAction(Request $request)
    {
        $id = $request->getParam('id');
        $result = !is_null($id) ? Result::getFromId($id) : new ResultDTO();
        return $this->viewBuilder->generatePart('result_view.php', $result);
    }
}