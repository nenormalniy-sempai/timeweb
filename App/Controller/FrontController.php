<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 10:59
 */

namespace App\Controller;

class FrontController extends AbstractController
{
    public function indexAction(): string
    {
        return $this->viewBuilder->generateView('main_view.php', 'template_view.php');
    }
}