<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 14:46
 */

namespace App\Controller;

use App\Client\ClientCurl;
use App\Kernel\Interfaces\RequestInterface;
use App\Model\Search;

class SearchController extends AbstractController
{
    const SEARCH_URL = 'search';

    public function indexAction(RequestInterface $request)
    {
        $model = new Search($request, new ClientCurl());
        return $this->viewBuilder->generatePart('result_view.php', $model->search());
    }
}