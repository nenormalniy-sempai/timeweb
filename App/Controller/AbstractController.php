<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 13:20
 */

namespace App\Controller;

use App\View\Builder\ViewBuilder;

abstract class AbstractController
{
    /**
     * @var ViewBuilder
     */
    protected $viewBuilder;

    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $this->viewBuilder = new ViewBuilder();
    }
}