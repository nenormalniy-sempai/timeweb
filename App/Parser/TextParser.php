<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 21:42
 */

namespace App\Parser;

class TextParser extends AbstractParser implements ParserInterface
{
    /**
     * @var string
     */
    private $text;

    /**
     * TextParser constructor.
     *
     * @param string $text
     * @param string $url
     * @param string $type
     */
    public function __construct(string $text, string $url, string $type)
    {
        parent::__construct($url, $type);
        $this->text = $text;
    }

    public function parse(string $page): ResultDTO
    {
        $result = new ResultDTO();
        $count = substr_count($page, $this->text);
        $elements = $count ? [$this->text] : [];
        $result->setElements($elements);
        $result->setCount($count);
        $result->setUrl($this->url);

        return $result;
    }
}