<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 21:39
 */

namespace App\Parser;

use App\Model\Search;

abstract class ParserFactory
{
    /**
     * Возвращает требуемый парсер, взависимости от типа
     *
     * @param string $type
     * @param string $url
     * @param string|null $text
     * @return ParserInterface
     */
    public static function getParser(string $type, string $url, $text): ParserInterface
    {
        switch ($type) {
            case Search::TYPE_LINKS:
                return new TagParser(TagParser::LINK_REGEXP, $url, $type);
            case Search::TYPE_IMAGES:
                return new TagParser(TagParser::IMAGE_REGEXP, $url, $type);
            case Search::TYPE_TEXTS:
                return new TextParser($text, $url, $type);
        }
    }
}