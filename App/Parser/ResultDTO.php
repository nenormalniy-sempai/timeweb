<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 18.08.19
 * Time: 15:01
 */

namespace App\Parser;

class ResultDTO
{
    /**
     * @var array
     */
    private $elements = [];

    /**
     * @var int
     */
    private $count = 0;

    /**
     * @var string
     */
    private $url;

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param array $elements
     */
    public function setElements(array $elements): void
    {
        $this->elements = $elements;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * Проверяет, пустой ли объект
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->elements);
    }
}