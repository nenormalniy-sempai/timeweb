<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 21:40
 */

namespace App\Parser;

interface ParserInterface
{
    public function parse(string $page): ResultDTO;
}