<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 17.08.19
 * Time: 0:33
 */

namespace App\Parser;

use App\Model\Db\Result;

abstract class AbstractParser
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $type;

    /**
     * AbstractParser constructor.
     *
     * @param string $url
     * @param string $type
     */
    public function __construct(string $url, string $type)
    {
        $this->type = $type;
        $urlScheme = parse_url($url);

        if (!empty($urlScheme)) {
            $this->url = $urlScheme['scheme'] . '://' . $urlScheme['host'];
        }
    }
}