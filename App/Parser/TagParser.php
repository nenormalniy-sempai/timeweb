<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 22:07
 */

namespace App\Parser;

class TagParser extends AbstractParser implements ParserInterface
{
    const LINK_REGEXP = '<a\s[^>]*href=("??)([^" >]*?)\\1[^>]*>(.*)<\/a>';
    const IMAGE_REGEXP = '<img\s[^>]*src=("??)([^" >]*?)\\1[^>]*>';

    /**
     * @var string
     */
    private $regexp;

    /**
     * TagParser constructor.
     *
     * @param string $regexp
     * @param string $url
     * @param string $type
     */
    public function __construct(string $regexp, string $url, string $type)
    {
        parent::__construct($url, $type);
        $this->regexp = $regexp;
    }

    public function parse(string $page): ResultDTO
    {
        $data = [];
        $result = new ResultDTO();

        if (preg_match_all("/$this->regexp/siU", $page, $matches)) {
            if (empty($matches[2])) {
                return $result;
            }
            foreach ($matches[2] as $match) {
                if ($match == '#' || empty($match)) {
                    continue;
                }

                if (mb_substr($match, 0, 1) == '/') {
                    $match = $this->url . $match;
                }

                $data[] = $match;
            }
        }

        $elements = array_unique($data);

        $result->setElements($elements);
        $result->setCount(count($elements));
        $result->setUrl($this->url);

        return $result;
    }
}