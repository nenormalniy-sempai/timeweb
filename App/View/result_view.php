<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 18.08.19
 * Time: 15:26
 */

/**
 * @var \App\Parser\ResultDTO $data
 */
?>
<?php if (!$data->isEmpty()): ?>
    <h2>URL: <?= $data->getUrl(); ?></h2>
    <h2>Количество элементов: <?= $data->getCount(); ?></h2>
    <?php $elements = $data->getElements(); ?>

    <?php foreach ($elements as $element): ?>
        <?= $element . '<br>'; ?>
    <?php endforeach; ?>
<?php else: ?>
    Не найдены элементы поиска.
<?php endif; ?>