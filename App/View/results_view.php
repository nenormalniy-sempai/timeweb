<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 18.08.19
 * Time: 16:54
 */

/**
 * @var array $data
 */
?>

<div class="modal" tabindex="-1" id="result" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Результат поиска</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-content"></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>

<?php if (empty($data)): ?>
    <h2>Нет результатов поиска.</h2>
<?php else: ?>
    <?php foreach ($data as $id => $elem): ?>
        <div>
            <span style="cursor:pointer;" class="result-row" data-value="<?= $id; ?>"><?= $elem['url']; ?></span>
            <span>Количество элементов: <?= $elem['count']; ?></span>
            <span>Поиск : <?= \App\Model\Search::getTypeName($elem['type']); ?></span>
        </div>
        <br>
    <?php endforeach; ?>
<?php endif; ?>