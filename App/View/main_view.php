<?php
use App\Model\Search;
?>

<form class="form-inline" role="form" id="search-form" method="POST" action="">
    <div class="form-group">
        <input type="text" name="url" class="form-control" id="input-url" required placeholder="Введите URL">
        <input type="radio" name="criteria" id="links" checked value="<?= Search::TYPE_LINKS; ?>">
        <label for="input-links">Поиск ссылок</label>
        <input type="radio" name="criteria" id="input-images" value="<?= Search::TYPE_IMAGES; ?>">
        <label for="input-images">Поиск картинок</label>
        <input type="radio" name="criteria" id="input-texts" value="<?= Search::TYPE_TEXTS; ?>">
        <label for="input-texts">Поиск текста</label>
        <input type="text" hidden name="criteria-type" id="criteria-type" value="<?= Search::TYPE_LINKS; ?>">
        <textarea style="display:none;" name="text-search" class="form-control" id="text-search"
                  placeholder="Введите текст"></textarea>
    </div>
    <button type="submit" class="btn btn-default">Поиск</button>
</form>

<div id="content"></div>