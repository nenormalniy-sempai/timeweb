<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 13:22
 */

namespace App\View\Builder;

class ViewBuilder
{
    function generateView(string $contentView, string $templateView, $data = null): string
    {
        return include_once __DIR__ . '/../' . $templateView;
    }

    function generatePart(string $contentView, $data = null): string
    {
        return include_once __DIR__ . '/../' . $contentView;
    }
}