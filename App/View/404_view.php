<?php
/**
 * @var string $data
 */
?>
<div class="container">
	<div class="row">
			<div class="col-md-12 text-center">
			<div class="alert alert-error">
				<h1>Что-то пошло не так...Вы уверены, что всё правильно сделали?</h1>
                <h2><?= $data; ?></h2>
			</div>
			<div class="form-group">
				<div class="col-md-12 text-center">
					<a href="/"><input class="btn btn-primary" value="Вернуться"></a>
				</div>
			</div>
		</div>
	</div>
</div>