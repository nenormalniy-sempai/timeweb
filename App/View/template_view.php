<?php
/**
 * @var $contentView string
 */

use App\Controller\SearchController;
use App\Controller\ResultController;
use App\Model\Search;

?>

<html>
<head>
    <title>Задание</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <script language="JavaScript" type="text/javascript" src="/assets/js/jquery.js"></script>
    <script language="JavaScript" type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<h2><a href="/">Поиск</a></h2>
<h2><a href="/<?= ResultController::RESULT_URL; ?>">Результаты</a></h2>

<?php @include_once $contentView; ?>
</body>
</html>

<script>
    $(document).ready(function () {
        $("#search-form").submit(function (event) {
            if (!isUrlValid($("#input-url").val())) {
                alert('Неправильно введён URL');
            }

            if ($("#text-search").val() == '' && $("#criteria-type").val() == <?= Search::TYPE_TEXTS; ?>) {
                alert('Заполните поле текста.');
            }

            $('#content').empty();

            $.ajax({
                url: "<?= SearchController::SEARCH_URL; ?>",
                type: "POST",
                dataType: "html",
                data: $(this).serialize(),
                success: function (result) {
                    $('#content').html(result);
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });

            return false;
        });

        $("input:radio").click(function (event) {
            $("#criteria-type").val($(this).val());
            if ($(this).val() == <?= Search::TYPE_TEXTS; ?>) {
                $("#text-search").show();
                $("#text-search").prop("required", true);
            } else {
                $("#text-search").hide();
                $("#text-search").prop("required", false);
            }
        });

        function isUrlValid(userInput) {
            var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
            if (res == null) {
                return false;
            }

            return true;
        }

        $(".result-row").click(function (event) {
            var url = "<?= \App\Controller\ResultController::RESULT_ROW_URL; ?>";
            $('.modal-content').empty();
            $.ajax({
                type: 'GET',
                url: url + "?id=" + $(this).data("value"),
                success: function (output) {
                    $(".modal-content").append(output);
                    $('#result').modal('show');
                },
                error: function (output) {
                    alert("Не удалось выполнить запрос.");
                }
            });
        });
    });
</script>