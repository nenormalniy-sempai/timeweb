<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 15.08.19
 * Time: 18:14
 */

namespace App\Config;

class ConfigLoader
{
    private static $config = [];

    public static function init()
    {
        if (empty(self::$config)) {
            self::uploadConfig();
        }
    }

    private static function uploadConfig()
    {
        self::$config['db'] = DbConfig::getInstance();
    }

    public static function getDbConfig(): DbConfig
    {
        return self::$config['db'];
    }
}