<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 15.08.19
 * Time: 18:30
 */

namespace App\Config;

class DbConfig
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $db;

    /**
     * @var string
     */
    private $dbms;

    /**
     * @return string
     */
    public function getDbms(): string
    {
        return $this->dbms;
    }

    /**
     * @param string $dbms
     */
    public function setDbms(string $dbms): void
    {
        $this->dbms = $dbms;
    }

    /**
     * @return string
     */
    public function getDb(): string
    {
        return $this->db;
    }

    /**
     * @param string $db
     */
    public function setDb($db): void
    {
        $this->db = $db;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort(string $port)
    {
        $this->port = $port;
    }

    /**
     * @return DbConfig
     */
    public static function getInstance(): self
    {
        if (empty(self::$instance)) {
            self::$instance = self::uploadDbConfig();
        }

        return self::$instance;
    }

    /**
     * Загрузка конфигурации
     *
     * @return DbConfig
     */
    private static function uploadDbConfig(): self
    {
        $instance = new self();
        $instance->setUser(getenv('DB_USER', 'root'));
        $instance->setPassword(getenv('DB_PASSWORD', 'qwerty'));
        $instance->setHost(getenv('DB_HOST', '127.0.0.1'));
        $instance->setPort(getenv('DB_PORT', '3306'));
        $instance->setDb(getenv('DB_DATABASE', 'mvc'));
        $instance->setDbms(getenv('DB_DBMS', 'mysql'));

        return $instance;
    }

    /**
     * Возвращает соединение с базой
     *
     * @param bool $withDb
     * @return \PDO
     */
    public static function getPDO(bool $withDb = false): \PDO
    {
        $db = $withDb ? ';dbname=' . self::$instance->getDb() : '';
        return new \PDO(
            self::$instance->getDbms() .':host=' . self::$instance->getHost()
            . ';port=' . self::$instance->getPort() . $db,
            self::$instance->getUser(),
            self::$instance->getPassword()
        );
    }
}