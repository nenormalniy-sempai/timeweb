<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 22:36
 */

namespace App\Config;

class DbCreator
{
    /**
     * @var DbConfig
     */
    private $dbConfig;

    /**
     * DbCreator constructor.
     *
     * @param DbConfig $config
     */
    public function __construct(DbConfig $config)
    {
        $this->dbConfig = $config;
    }

    /**
     * Создаёт бд и таблицу
     */
    public function createDb()
    {
        $dbh = DbConfig::getPDO();

        $dbh->exec('CREATE DATABASE IF NOT EXISTS ' . $this->dbConfig->getDb() .  ';');
        $dbh->exec('CREATE TABLE IF NOT EXISTS ' . $this->dbConfig->getDb()
            .  '.results (id int NOT NULL AUTO_INCREMENT PRIMARY KEY,url varchar(255), data text, count int, type tinyint);');
    }
}