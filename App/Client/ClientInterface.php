<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 18:49
 */

namespace App\Client;

interface ClientInterface
{
    public function sendRequest(string $url): ?string;
}