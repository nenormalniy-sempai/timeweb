<?php
/**
 * Created by PhpStorm.
 * User: nenormalka
 * Date: 16.08.19
 * Time: 18:50
 */

namespace App\Client;

class ClientCurl implements ClientInterface
{
    /**
     * Курл
     *
     * @var resource
     */
    private $curl;

    private const CURL_OPTIONS = [
        CURLOPT_CONNECTTIMEOUT => 60,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_POST => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FOLLOWLOCATION => 1,
    ];

    /**
     * Инициализация курла
     *
     * @param string $url
     */
    private function init(string $url)
    {
        $this->curl = curl_init();
        $options = [
            CURLOPT_URL => $url,
        ];

        curl_setopt_array($this->curl, self::CURL_OPTIONS + $options);
    }

    public function __destruct()
    {
        if (empty($this->curl)) {
            return;
        }
        curl_close($this->curl);
    }

    /**
     * Запрос странички
     *
     * @param string $url
     * @throws \Exception
     * @return string
     */
    public function sendRequest(string $url): ?string
    {
        try {
            $this->init($url);
            $response = curl_exec($this->curl);

            if (curl_errno($this->curl) != 0 || empty($response)) {
                return null;
            }

            return $response;
        } catch (\Throwable $exception) {
            return null;
        }
    }
}