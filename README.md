1. Добавить в hosts: mvc.loc 127.0.0.1
2.1 Развёртывание через докер:
Если docker и docker-compose уже установлены, то можно перейти сразу к шагу №: 2.1.11
2.1.1 Установка docker и docker-compose для ubuntu


2.1.2 Обновляем требуемые инструменты:
````
sudo apt update
sudo apt-get install -y apt-transport-https software-properties-common ca-certificates curl wget
````

2.1.3 Добавляем в репозитории ключ докера:
````
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
````

2.1.4 Добавляем репозиторий в список:
````
echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic nightly" | sudo tee /etc/apt/sources.list.d/docker.list
````

2.1.5 Обновляем кэш репозиториев:
````
sudo apt update
````

2.1.5 Проверяем, что репозиторий официальный:
````
sudo apt-cache policy docker-ce
````

Вывод должен быть похож на это:
````
docker-ce:
 Installed: (none)
 Candidate: 18.06.0~ce~dev~git20180504.170722.0.0e0adf0-0~ubuntu
 Version table:
 18.06.0~ce~dev~git20180504.170722.0.0e0adf0-0~ubuntu 500
 500 https://download.docker.com/linux/ubuntu bionic/nightly amd64 Packages
 18.06.0~ce~dev~git20180503.170717.0.9bc9d40-0~ubuntu 500
 500 https://download.docker.com/linux/ubuntu bionic/nightly amd64 Packages
````

2.1.6 Устанавливаем докер и проверяем его версию:
````
sudo apt -y install docker-ce
docker --version
````

Вывод должен быть похож на это:
````
Docker version 18.06.0-ce-dev, build 0e0adf0
````

2.1.7 Устанавливаем Docker-compose:
````
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
````

2.1.8 Добавляем права на запуск:
````
sudo chmod +x /usr/local/bin/docker-compose
````

2.1.9 Проверяем версию:
````
docker-compose --version
````

Вывод должен быть похож на это:
````
docker-compose version 1.21.2, build 1719ceb
````

2.1.10 Разрешаем не root пользователю запускать Docker:
````
sudo groupadd docker
sudo usermod -aG docker $(whoami)
````

2.1.11 Для развёртывание окружения требуется в текущей папке выполнить ``docker-compose up -d``.

2.1.12 Затем выполнить команду ``docker-compose exec php bash``

2.1.13 В открывшемся терминале требуется выполнить ``composer install``. После скачивания пакетов выполнить ``cp .env.example .env``.
Должен появиться файл .env с конфигурацией. И в финале выполнить ``php console.php``. В терминале должна появиться надпись:
``База и таблица созданы.`` (Выйти из консоли можно командой exit)

2.1.14 В браузере перейти по адресу http://mvc.loc:88



2.2.1 Развёртывание без докера. (я не проверял, но по идее никаких проблем с этим быть не должно)

2.2.2 Добавить в веб-сервер конфигурацию аналогичную ``docker/nginx/timeweb.conf``

2.2.3 Создать в корневом каталоге файл ``.env`` аналогичный ``.env.example``, но с вашими настройками окружения

2.2.4 Выполнить ``php console.php``. В терминале должна появиться надпись:
``База и таблица созданы.``

2.2.5 В браузере перейти по адресу http://mvc.loc